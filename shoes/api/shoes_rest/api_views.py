from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Shoe, BinVO
from common.json import ModelEncoder
import json


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "name",
        "manufacturer",
        "color",
        "picture"
        ]

    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}


class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["import_href", "closet_name"]


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "name",
        "manufacturer",
        "color",
        "picture",
        "bin"
        ]

    encoders = {
        "bin": BinVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
            safe=False,
        )

    else:
        content = json.loads(request.body)

        try:
            shoe_href = content["bin"]["import_href"]
            bin = BinVO.objects.get(import_href=shoe_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=404,
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )


# @require_http_methods(["GET", "DELETE"])
# def api_show_shoe(request, id):
#     if request.method == "GET":
#         show_shoe = Shoe.objects.filter(id=id)
#         return JsonResponse(
#             show_shoe,
#             encoder=ShoeDetailEncoder,
#             safe=False,
#         )
from django.db import models
from django.urls import reverse


class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)


class Shoe(models.Model):
    name = models.CharField(max_length=200)
    manufacturer = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture = models.URLField()

    bin = models.ForeignKey(
        BinVO,
        related_name="bin",
        on_delete=models.CASCADE
    )

    # def get_api_url(self):
    #     return reverse("api_show_shoe", kwargs={"id": self.id})

    def __str__(self):
        return self.name


# Create your models here.

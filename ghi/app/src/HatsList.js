import React, { useEffect, useState } from 'react';

const HatsList = () => {
  const [hats, setHats] = useState([]);

  const fetchData = async () => {
    const url = 'http://localhost:8090/api/hats/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setHats(data.hats);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Fabric</th>
          <th>Style</th>
          <th>Color</th>
          <th>Picture</th>
          <th>Location</th>
        </tr>
      </thead>
      <tbody>
        {hats.map(hat => (
            <tr key={hat.href}>
              <td>{hat.name}</td>
              <td>{hat.fabric}</td>
              <td>{hat.style}</td>
              <td>{hat.color}</td>
              <td>{hat.picture}</td>
              <td>{hat.location}</td>
            </tr>
        ))}
      </tbody>
    </table>
  );
};

export default HatsList;
